/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
*/



const data = require("./data.json");
const fs = require("fs");




// 1. Retrieve data for ids : [2, 13, 23].


const getDataForIDs = (data, callback) => {


    let err = false;
    if (err) {
        callback(err);
    }
    else {
        const getDataForID = data.employees.filter((items) => {
            if (items.id == 2 || items.id == 13 || items.id == 23) {
                return true;
            }
            else {
                return false;
            }
        });
        fs.writeFile("getDataForIDs.json", JSON.stringify(getDataForID), (err) => {
            if (err) {
                callback(err);
            }
            else {
                const groupData = data.employees.reduce((acc, current) => {
                    if (current.company == "Powerpuff Brigade") {
                        acc.push(current)
                    }
                    return acc;
                }, []);
                fs.writeFile("getGroupData.json", JSON.stringify(groupData), (err) => {
                    if (err) {
                        callback(err);
                    }
                })

            }

        });
    }
    function callback(err, data) {
        if (err) {
            console.error("Error");
        }
        else {
            console.log(data);
        }
    }
};

getDataForIDs(data);





